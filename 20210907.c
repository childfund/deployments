https://app.gearset.com/finished?deploymentId=9c550eaa-29de-4027-9246-44b4c69c4005

CFD-1229	Samson Murage	"
Report Folder:
Sponsorship Reports

Report:
Legacy Sponsorships - Monthly Cancels

Profile:
CFI Finance - Accounting

Object Settings:
Reports - Default on

System Permissions:
Export Reports
Run Reports
View Reports in Public Folders

Profile:
CFI Finance - Treasury

Object Settings:
Reports - Default on

System Permissions:
Export Reports
Run Reports
View Reports in Public Folders"

CFD-2204	Corey Valentyne	"
Apex:
SponsorshipUpdateFlowAction
SponsorshipUpdateFlowActionTest"


CFD-2241	Samson Murage	"Object:
Case

Fields:
Contact__c
Mailing_Address__c
Preferred_Email__c

Page Layout:
Case Layout
Child Materials Expedite Layout
Child Materials Issue Layout
Country Office Request Layout
Master Case Layout
Master Case SC Data Layout
Participant Incident Death Layout
Participant Incident Duplicate Layout
Participant Incident Safeguarding Layout
Participant Incident Special Request Layout
Reserve Request Layout
Routed Case Layout
Routed Case Layout - Alliance Community"


CFD-2253	Alexandra Ervan	"
Community:
Local Partner

Pages:
Participant Detail (all versions)
Mail Control Slip Detail
Mail Control Slip List

Object:
Participant

Page Layouts:
Participant Layout
Participant Layout - Alliance Community

Flow:
Mail Control Slip - Create"

CFD-2296	Gui Manders	"
ApexClass
*	ParticipantDepartureController
*	ParticipantDepartureControllerTest
*	Utilities

CustomField
*	Sponsorship_Preference__c.Preference_Value__c

Flow
*	Participant_Child_Departure

LightningComponentBundle
*	flowFilteredFieldMulti"


CFD-2393	Corey Valentyne	"
LWC:
sponsorshipPreferenceMassUpdate

Apex:
SponsorshipPreferenceUpdateCtrl

Profile:
CFI Sponsorship
CFI Administrator
System Administrator"


CFD-2406	Mike Quackenbush	"
Object:
Recurring Donation

Validation Rule:
Do_Not_Substitute_Reason


-Updated Flow:
Recurring_Donation_Manage_Sponsorship



CHANGE SET #2:
-Updated Flow:
Recurring_Donation_Create_New_Sponsorship"
CFD-2443	Mike Quackenbush	"* Flow:
Recurring_Donation_Manage_Sponsorship"